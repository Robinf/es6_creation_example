var gulp = require('gulp'),
    eslint = require('gulp-eslint'),
    connect = require('gulp-connect'),
    sass = require('gulp-sass');

var colors = require('colors');
var codeOK = false;

gulp.task('lint', function () {
    return gulp.src(['app/js/**/*.js'])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('sass', function () {
    return gulp.src(['app/styles/**/*.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('.tmp/styles/'))
});

gulp.task('reload', function () {
    gulp.src('./app')
        .pipe(connect.reload());
});

gulp.task('serve', ['sass'], function () {
    connect.server({
        root: ['app', '.tmp'],
        port: '8001',
        livereload: true,
        middleware: function (connect) {
            return [
                connect().use(
                    '/jspm_packages',
                    connect.static('./jspm_packages')
                )
            ]
        }
    });

    gulp.watch(['./app/js/**/*.js'], function (event) {
        gulp.src(event.path)
            .pipe(eslint({
                rules: {
                    'no-console': 0 //Allow the console, this should be removed for production env
                }
            }))
            .pipe(eslint.format())
            .pipe(eslint.failOnError())
            .on('finish', function(){
                if(!codeOK) {
                    console.log('[ESLint] Your code looks OK!'.green);
                    codeOK = true;
                }
            })
            .on('error', function(){
                codeOK = false;
                console.log('[ESLint] The following problems were found in your code:'.red)
            })
            .pipe(connect.reload());
    });

    gulp.watch(['./app/*.html'], function (event) {
        gulp.src(event.path)
            .pipe(connect.reload());
    });

    gulp.watch(['./app/styles/**/*.scss'], ['sass', 'reload']);
});

gulp.task('default', ['lint'], function () {
    console.log('Everything is cool');
});