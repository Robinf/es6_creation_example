'use strict';

import Character from './character';
import $ from 'jquery';

const PLAYER_SPEED = 0.4; //Should be the player speed per ms

export default class extends Character {
    constructor(playground) {
        super(playground);

        this.element.css('backgroundColor', 'blue');

        this.velocity = {x: 0, y: 0};

        //Setup keys
        this.keys = {
            UP: false,
            DOWN: false,
            LEFT: false,
            RIGHT: false
        };
    }

    setupEvents() {
        $(document).on('keydown', evt=> {
            switch (evt.keyCode) {
                case 87:
                    //W
                    this.keys.UP = true;
                    break;
                case 83:
                    //S
                    this.keys.DOWN = true;
                    break;
                case 65:
                    //A
                    this.keys.LEFT = true;
                    break;
                case 68:
                    //D
                    this.keys.RIGHT = true;
                    break;

                case 107:
                    //+
                    let increase = 10;

                    this.width += increase;
                    this.height += increase;

                    this.top -= increase / 2;
                    this.left -= increase / 2;
                    break;
                case 109:
                    //-
                    let decrease = 10;

                    this.width -= decrease;
                    this.height -= decrease;

                    this.top += decrease / 2;
                    this.left += decrease / 2;

                    break;
                case 32:
                    //Space
                    //Use the element to trigger an event so we don't have to get a PubSub system just for this
                    this.element.trigger('superShoot');
                    break;
                default:
                    console.log('Unrecognized key code: ', evt.keyCode);
            }
        });

        $(document).on('keyup', evt=> {
            switch (evt.keyCode) {
                case 87:
                    //W
                    this.keys.UP = false;
                    break;
                case 83:
                    //S
                    this.keys.DOWN = false;
                    break;
                case 65:
                    //A
                    this.keys.LEFT = false;
                    break;
                case 68:
                    //D
                    this.keys.RIGHT = false;
                    break;
            }
        });
    }

    loop(deltaTime) {
        if (this.keys.UP && !this.keys.DOWN) {
            this.velocity.y = -PLAYER_SPEED;
        } else if (this.keys.DOWN && !this.keys.UP) {
            this.velocity.y = PLAYER_SPEED;
        } else {
            this.velocity.y = 0;
        }

        if (this.keys.LEFT && !this.keys.RIGHT) {
            this.velocity.x = -PLAYER_SPEED;
        } else if (this.keys.RIGHT && !this.keys.LEFT) {
            this.velocity.x = PLAYER_SPEED;
        } else {
            this.velocity.x = 0;
        }

        let newTop = this.top + (this.velocity.y * deltaTime);
        let newLeft = this.left + (this.velocity.x * deltaTime);

        if (newTop < 0) {
            newTop = 0;
        } else if (newTop > $('body').height() - this.height) {
            newTop = $('body').height() - this.height;
        }

        if (newLeft < 0) {
            newLeft = 0;
        } else if (newLeft > $('body').width() - this.width) {
            newLeft = $('body').width() - this.width;
        }

        this.top = newTop;
        this.left = newLeft;
    }
}
