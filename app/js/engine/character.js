'use strict';

import $ from 'jquery';
import ScreenElement from './screen_element';

export default class extends ScreenElement {
    constructor(playground) {
        super();

        //Create element
        this.element = $('<div class="character"></div>');
        playground.append(this.element);

        //Set styling
        this.element.css({
            width: 50,
            height: 50,
            backgroundColor: '#FF0000',
            position: 'absolute'
        });

        this.randomizeLocation();

        this.setupEvents();
    }

    setupEvents() {
        this.element.on('click', this.randomizeLocation.bind(this));

        this.element.hover(()=> {
            this.element.css({
                backgroundColor: '#00FF00'
            });
        }, ()=> {
            this.element.css({
                backgroundColor: '#FF0000'
            });
        });
    }

    randomizeLocation(animate = false) {
        if (!animate) {
            this.element.css({
                left: Math.random() * (window.innerWidth - 50),
                top: Math.random() * (window.innerHeight - 50)
            });
        } else {
            this.element.animate({
                left: Math.random() * (window.innerWidth - 50),
                top: Math.random() * (window.innerHeight - 50)
            });
        }
    }

    lightUp() {
        if (this['lightUpTimeout']) {
            window.clearTimeout(this['lightUpTimeout']);
        }

        this.element.css({
            backgroundColor: 'gray'
        });

        this['lightUpTimeout'] = window.setTimeout(()=> {
            this.element.css({
                backgroundColor: '#FF0000'
            });
        }, 100);
    }
}
