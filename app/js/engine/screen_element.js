'use strict';

export default class {
    get top() {
        return parseInt(this.element.css('top'));
    }

    set top(top) {
        this.element.css('top', top);
    }

    get left() {
        return parseInt(this.element.css('left'));
    }

    set left(left) {
        this.element.css('left', left);
    }

    get width() {
        return this.element.width();
    }

    set width(width) {
        this.element.width(width);
    }

    get height() {
        return this.element.height();
    }

    set height(height) {
        this.element.height(height);
    }
}
