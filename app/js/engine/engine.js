'use strict';

import $ from 'jquery';
import Character from './character';
import Player from 'engine/player';
import Bullet, {BULLET_SPEED} from 'engine/bullet';
import FpsCounter from './fps_counter';

export default class Engine {
    constructor() {
        //Init variables
        this.characters = [];
        this.bullets = [];
        this.fpsCounter = null;
        this.player = null;

        this.lastDate = new Date().getTime();

        this.createPlayground();
        this.createTestCharacters();
        this.createFpsCounter();
        this.createPlayer();

        this.setupLoop();
        this.setupEvents();
    }

    createPlayground() {
        this.element = $('<div class="playground"></div>');
        this.element.css({
            position: 'relative',
            transform: 'translate3d(0,0,0)'
        });

        $('body').append(this.element);
    }

    createTestCharacters() {
        for (let i = 0; i < 5; i++) {
            this.characters.push(new Character(this.element));
        }
    }

    createFpsCounter() {
        this.fpsCounter = new FpsCounter();
    }

    createPlayer() {
        this.player = new Player(this.element);
    }

    createBullet(startPoint, velocity) {
        this.bullets.push(new Bullet(this.element, startPoint, velocity));
    }

    setupLoop() {
        window.requestAnimationFrame(()=> {
            this.loop();
            this.setupLoop();
        });
    }

    loop() {
        let newDate = new Date().getTime();
        let deltaTime = newDate - this.lastDate;

        this.fpsCounter.loop(deltaTime);
        this.player.loop(deltaTime);

        this.bullets.forEach((bullet, index)=> {
            bullet.loop(deltaTime);

            if (bullet.timeExisted > 5000 ||
                (bullet.top < 0 || bullet.left < 0 || bullet.top > $('body').height() - bullet.height || bullet.left > $('body').width() - bullet.width)) {
                bullet.destroy();
                this.bullets.splice(index, 1);
            }

            this.characters.forEach(char=> {
                if (Engine.checkCollision(bullet, char)) {
                    char.lightUp();
                }
            });
        });

        this.lastDate = newDate;
    }

    setupEvents() {
        $(document).on('click', evt=> {
            let speed = BULLET_SPEED;

            //TODO: subtract playground location from x and y coordinates
            let x = evt.pageX;
            let y = evt.pageY;

            let xDiff = x - (this.player.left + (this.player.width / 2));
            let yDiff = y - (this.player.top + (this.player.height / 2));

            let c = Math.sqrt(Math.pow(xDiff, 2) + Math.pow(yDiff, 2));
            let d = speed / c;

            let velocity = {
                x: (xDiff * d),
                y: (yDiff * d)
            };

            this.createBullet({x: this.player.left + (this.player.width / 2), y: this.player.top + (this.player.height / 2)}, velocity);
        });

        this.player.element.on('superShoot', ()=> {
            //Space

            //Fire 8 bullets in different directions
            let x = this.player.left + (this.player.width / 2),
                y = this.player.top + (this.player.height / 2),
                speed = BULLET_SPEED;

            this.createBullet({x, y}, {x: 0, y: -speed}); //top
            this.createBullet({x, y}, {x: 0, y: speed}); //bottom
            this.createBullet({x, y}, {x: -speed, y: 0}); //left
            this.createBullet({x, y}, {x: speed, y: 0}); //right
            this.createBullet({x, y}, {x: -speed / 2, y: -speed / 2}); //top left
            this.createBullet({x, y}, {x: speed / 2, y: -speed / 2}); //top right
            this.createBullet({x, y}, {x: -speed / 2, y: speed / 2}); //bottom left
            this.createBullet({x, y}, {x: speed / 2, y: speed / 2}); //bottom right
        });
    }

    static checkCollision(rect1, rect2) {
        //TODO: optimize
        return rect1.left < rect2.left + rect2.width &&
            rect1.left + rect1.width > rect2.left &&
            rect1.top < rect2.top + rect2.height &&
            rect1.height + rect1.top > rect2.top;
    }
}
