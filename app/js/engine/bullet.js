'use strict';

import ScreenElement from './screen_element';
import $ from 'jquery';

export const BULLET_SPEED = 0.4;

export default class extends ScreenElement {
    constructor(playground, startPoint, velocity) {
        super();

        this.element = $('<div class="bullet"></div>');
        this.element.css({
            width: 5,
            height: 5,
            background: 'orange',
            position: 'absolute'
        });
        playground.append(this.element);


        this.left = startPoint.x;
        this.top = startPoint.y;

        this.velocity = velocity;

        this.timeExisted = 0;
    }

    loop(deltaTime) {
        this.timeExisted += deltaTime;

        this.left += this.velocity.x * deltaTime;
        this.top += this.velocity.y * deltaTime;
    }

    destroy(){
        this.element.remove();
    }
}
