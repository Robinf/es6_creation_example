'use strict';

import $ from 'jquery';

export default class {
    constructor() {
        this.element = $('<div class="fps-counter"></div>');
        this.element.css({
            position: 'absolute',
            left: 0,
            top: 0,
            padding: 5
        });

        $('body').append(this.element);
    }

    loop(deltaTime) {
        let fps = parseInt(1000 / deltaTime);

        this.element.text(`FPS: ${fps}`);
    }
}
