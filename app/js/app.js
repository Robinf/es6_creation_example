'use strict';

import Engine from 'engine/engine';

export class App {
    constructor() {
        this.engine = new Engine();
    }
}
