# ES6 game test
This is a game test which showcases some of the new ES6 ways of implementing stuff.

## How to build
Install [NodeJS](https://nodejs.org/) to get NPM, then run ``npm install``. This will install the dependencies required to run the linting and the server.

The next step is to make sure you have [JSPM](http://jspm.io/) installed, then run: ``jspm install``. That will install all dependencies required for the frontend application.

You will also need [Gulp](http://gulpjs.com/) to run a server.

## How to run
After having installed the dependencies, simply run ``gulp serve`` to start the server and navigate to http://localhost:8001.